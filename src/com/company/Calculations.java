package com.company;

public class Calculations {
    static Point2D positionGeometricCenter(Point2D[] points) {
        double center_x = 0;
        double center_y = 0;
        int pointsNum = points.length;

        for (Point2D point: points) {
            center_x = center_x + point.getX();
        }

        for (Point2D point: points) {
            center_y = center_y + point.getY();
        }

        return new Point2D(center_x / pointsNum,center_y / pointsNum);
    }

    static Point2D positionCenterOfMass(MaterialPoint2D[] materialPoints) {
        double center_x = 0;
        double center_y = 0;
        double mass = 0;

        for (MaterialPoint2D materialPoint: materialPoints) {
            center_x = materialPoint.getMass() * materialPoint.getX();
            center_y = materialPoint.getMass() * materialPoint.getY();
            mass += materialPoint.getMass();
        }

        return new MaterialPoint2D(center_x/mass,center_y/mass, mass);
    }
}
